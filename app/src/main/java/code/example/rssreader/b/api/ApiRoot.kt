package code.example.rssreader.b.api

import code.example.rssreader.b.Constants
import code.example.rssreader.b.api.interceptors.LoggingInterceptor
import okhttp3.OkHttpClient
import org.androidannotations.annotations.AfterInject
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EBean
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit

@EBean(scope = EBean.Scope.Singleton)
open class ApiRoot {

    @Bean
    lateinit var loggingInterceptor: LoggingInterceptor

    lateinit var apiRss: ApiRss
        private set

    lateinit var apiFile: ApiFile
        private set

    @AfterInject
    internal fun init() {
        val rxAdapter = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io())
        val builder = Retrofit.Builder()
                .baseUrl(Constants.Settings.DEFAULT_RSS_HOST)
                .addCallAdapterFactory(rxAdapter)
                .addConverterFactory(SimpleXmlConverterFactory.create())

        val httpClient = OkHttpClient.Builder()
        httpClient.connectTimeout(Constants.Api.CONNECT_TIMEOUT.toLong(), TimeUnit.SECONDS)
        httpClient.readTimeout(Constants.Api.READ_TIMEOUT.toLong(), TimeUnit.SECONDS)
        httpClient.addInterceptor(loggingInterceptor)

        val retrofit = builder.client(httpClient.build()).build()

        apiRss = retrofit.create(ApiRss::class.java)
        apiFile = retrofit.create(ApiFile::class.java)
    }
}