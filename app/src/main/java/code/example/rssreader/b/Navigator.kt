package code.example.rssreader.b

import android.content.Context
import code.example.rssreader.b.activity.postBrowse.PostBrowseActivity_
import code.example.rssreader.b.activity.postPreview.PostPreviewActivity_
import code.example.rssreader.b.activity.settings.SettingsActivity
import code.example.rssreader.b.activity.settings.SettingsActivity_
import code.example.rssreader.b.model.rss.FeedItem
import org.androidannotations.annotations.EBean
import org.androidannotations.annotations.RootContext

@EBean
open class Navigator {

    @RootContext
    protected lateinit var context: Context

    /**
     * Navigation method to show PostPreviewActivity
     */
    fun gotoPostPreviewActivity(feedItem: FeedItem) {
        PostPreviewActivity_.intent(context).feedItem(feedItem).start()
    }

    /**
     * Navigation method to show PostBrowseActivity
     */
    fun gotoPostBrowseActivity(postUrl: String) {
        PostBrowseActivity_.intent(context).postUrl(postUrl).start()
    }

    /**
     * Navigation method to show SettingsActivity
     */
    fun gotoSettingsActivity() {
        SettingsActivity_.intent(context).start()
    }
}