package code.example.rssreader.b.activity.settings

import android.view.View
import android.widget.EditText
import android.widget.RelativeLayout
import code.example.rssreader.b.R
import code.example.rssreader.b.activity.BaseActivity
import org.androidannotations.annotations.*
import org.androidannotations.annotations.res.StringRes


/**
 * Setting activity class
 */
@EActivity(R.layout.activity_settings)
open class SettingsActivity : BaseActivity(), SettingsActivityContract.View {

    @StringRes(R.string.title_settings)
    override lateinit var toolbarTitle: String

    @ViewById(R.id.editText_hostUrl)
    protected lateinit var hostUrlTextView: EditText

    @ViewById(R.id.progressView)
    protected lateinit var progressView: RelativeLayout

    @Bean(SettingsActivityPresenter::class)
    override lateinit var presenter: SettingsActivityContract.Presenter

    @AfterViews
    protected fun afterViews() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        presenter.view = this
        presenter.loadSavedRssHost()
    }

    override fun setRssHostUrl(url: String) {
        hostUrlTextView.setText(url)
    }

    override fun setBusy(busy: Boolean) {
        progressView.visibility = if (busy) View.VISIBLE else View.GONE
    }

    @Click(R.id.button_save)
    fun onClick() {
        closeSoftKeyboard()

        val hostUrl = hostUrlTextView.text.toString()
        presenter.saveRssHost(hostUrl)
        presenter.checkRssHost(hostUrl)
    }
}
