package code.example.rssreader.b.api

import code.example.rssreader.b.model.rss.Feed
import retrofit2.http.GET
import retrofit2.http.Url
import rx.Observable

interface ApiRss {

    @GET
    fun getFeed(@Url feedUrl: String): Observable<Feed>
}