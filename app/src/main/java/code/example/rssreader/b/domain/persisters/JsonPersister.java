package code.example.rssreader.b.domain.persisters;

import android.support.annotation.Nullable;
import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.StringType;

import java.io.IOException;

public class JsonPersister<T> extends StringType {
    private ObjectMapper objectMapper;
    private Class<T> cls;

    protected JsonPersister(Class<T> cls) {
        super(SqlType.STRING, new Class<?>[]{cls});
        objectMapper = new ObjectMapper();
        this.cls = cls;
    }

    @Override
    public Object javaToSqlArg(FieldType fieldType, Object javaObject) {
        T myFieldClass = (T) javaObject;
        return myFieldClass != null ? getJsonFromMyFieldClass(myFieldClass) : null;
    }

    @Override
    public Object sqlArgToJava(FieldType fieldType, Object sqlArg, int columnPos) {
        return sqlArg != null ? getMyFieldClassFromJson((String) sqlArg) : null;
    }

    @Nullable
    private String getJsonFromMyFieldClass(T obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            Log.e(cls.getSimpleName(), "Exception while create Json from " + obj.getClass().getSimpleName());
            return null;
        }
    }

    @Nullable
    private T getMyFieldClassFromJson(String json) {
        try {
            return objectMapper.readValue(json, cls);
        } catch (IOException e) {
            Log.e(cls.getSimpleName(), "Exception while parsing Json to " + cls.getSimpleName());
            return null;
        }
    }

}
