package code.example.rssreader.b.domain.files

import android.content.Context
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EBean
import org.androidannotations.annotations.RootContext
import rx.Observable
import java.io.File
import java.net.URI

@EBean
open class FileManager {

    @RootContext
    protected lateinit var context: Context

    @Bean
    protected lateinit var fileLoader: FileLoader

    private val downloadDirPath: String
        get() {
            var downloadDirPath = context.filesDir.path
            val externalDownloadDirPathFile = context.getExternalFilesDir(null)
            if (externalDownloadDirPathFile != null)
                downloadDirPath = externalDownloadDirPathFile.path

            return downloadDirPath + File.separator
        }

    fun getFilePath(fileName: String): String =
            downloadDirPath + fileName.hashCode()

    fun isFileExists(fileName: String): Boolean =
            File(getFilePath(fileName)).exists()

    fun deleteFile(fileName: String): Boolean =
            File(getFilePath(fileName)).delete()

    fun loadFile(fileUrl: String,
                 fileName: String): Observable<URI?> =
            fileLoader.loadFile(fileUrl, getFilePath(fileName))


    private fun dirSize(dir: File): Long {
        var result: Long = 0
        if (dir.exists()) {
            val fileList = dir.listFiles()
            for (i in fileList.indices) {
                // Recursive call if it's a directory
                if (fileList[i].isDirectory) {
                    result += dirSize(fileList[i])
                } else {
                    // Sum the file size in bytes
                    result += fileList[i].length()
                }
            }
        }
        return result
    }
}