package code.example.rssreader.b.activity.main

import code.example.rssreader.b.model.rss.FeedItem
import com.arellomobile.mvp.MvpView

interface MainActivityContract {

    interface View : MvpView {

        var presenter: Presenter

        fun setBusy(busy: Boolean)

        fun setRefreshBusy(busy: Boolean)

        fun setRssItems(feedItems: List<FeedItem>)

        fun showSnackBarMessage(message: String)
    }

    interface Presenter {

        var view: View?

        fun loadData()

        fun refreshData()

        fun showSettings()

        fun showPostPreview(feedItem: FeedItem)
    }
}