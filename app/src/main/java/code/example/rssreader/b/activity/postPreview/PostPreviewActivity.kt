package code.example.rssreader.b.activity.postPreview

import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.widget.TextView
import code.example.rssreader.b.R
import code.example.rssreader.b.activity.BaseActivity
import code.example.rssreader.b.model.rss.FeedItem
import code.example.rssreader.b.view.LoadableImageView
import org.androidannotations.annotations.*
import org.androidannotations.annotations.res.StringRes

/**
 * Post preview activity class
 */

@EActivity(R.layout.activity_post_preview)
open class PostPreviewActivity : BaseActivity(), PostPreviewActivityContract.View {

    @StringRes(R.string.title_post_preview)
    override lateinit var toolbarTitle: String

    @Extra
    @JvmField
    protected var feedItem: FeedItem? = null

    @ViewById(R.id.imageView_postPreview)
    protected lateinit var imageView: LoadableImageView

    @ViewById(R.id.textView_title)
    protected lateinit var titleTextView: TextView

    @ViewById(R.id.textView_date)
    protected lateinit var pubDateTextView: TextView

    @ViewById(R.id.textView_author)
    protected lateinit var authorTextView: TextView

    @ViewById(R.id.textView_description)
    protected lateinit var descriptionTextView: TextView

    @Bean(PostPreviewActivityPresenter::class)
    override lateinit var presenter: PostPreviewActivityContract.Presenter

    @AfterViews
    protected fun afterViews() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        presenter.view = this
        presenter.proceedData(feedItem ?: return)
    }

    @Click(R.id.textView_title)
    fun onClick() {
        presenter.browseRssPost(feedItem?.link ?: return)
    }

    override fun setImageUrl(url: String?) {
        imageView.setUrl(url)
    }

    override fun setTitle(title: String) {
        val content = SpannableString(title)
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        titleTextView.text = content
    }

    override fun setPublicationDate(formattedPubDate: String) {
        pubDateTextView.text = formattedPubDate
    }

    override fun setAuthor(author: String) {
        authorTextView.text = author
    }

    override fun setDescription(description: String) {
        descriptionTextView.text = description
    }
}