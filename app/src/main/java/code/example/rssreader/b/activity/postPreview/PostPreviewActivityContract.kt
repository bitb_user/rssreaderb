package code.example.rssreader.b.activity.postPreview

import code.example.rssreader.b.model.rss.FeedItem

interface PostPreviewActivityContract {

    interface View {

        var presenter: Presenter

        fun setImageUrl(url: String?)

        fun setTitle(title: String)

        fun setPublicationDate(formattedPubDate: String)

        fun setAuthor(author: String)

        fun setDescription(description: String)
    }

    interface Presenter {

        var view: View?

        fun proceedData(feedItem: FeedItem)

        fun browseRssPost(postUrl: String)
    }
}