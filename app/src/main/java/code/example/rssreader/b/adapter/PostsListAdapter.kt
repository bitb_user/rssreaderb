package code.example.rssreader.b.adapter

import android.content.Context
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import code.example.rssreader.b.model.rss.FeedItem
import code.example.rssreader.b.view.PostItemView
import code.example.rssreader.b.view.PostItemView_
import org.androidannotations.annotations.EBean

@EBean
open class PostsListAdapter : BaseRecyclerViewAdapter<FeedItem, PostItemView>() {

    override fun onCreateItemView(parent: ViewGroup, viewType: Int): PostItemView {
        val postItemView = PostItemView_.build(parent.context)
        setupViewParams(parent.context, postItemView)
        return postItemView
    }

    override fun onBindViewHolder(viewHolder: ViewWrapper<PostItemView>, position: Int) {
        viewHolder.view.setFeedItem(items[position])
    }

    private fun setupViewParams(context: Context, view: View) {
        val pxMargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8.toFloat(),
                context.resources.displayMetrics).toInt()
        val layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        layoutParams.setMargins(pxMargin, pxMargin, pxMargin, pxMargin)
        view.layoutParams = layoutParams
    }
}
