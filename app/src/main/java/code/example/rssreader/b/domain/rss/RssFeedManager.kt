package code.example.rssreader.b.domain.rss

import code.example.rssreader.b.domain.rss.providers.OfflineRssDataProvider
import code.example.rssreader.b.domain.rss.providers.OnlineRssDataProvider
import code.example.rssreader.b.domain.rss.providers.RssDataProvider
import code.example.rssreader.b.model.rss.FeedItem
import code.example.rssreader.b.util.NetUtils
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EBean
import rx.Observable

/**
 * Rss feed manager
 */
@EBean
open class RssFeedManager {

    @Bean
    protected lateinit var netUtils: NetUtils

    @Bean
    protected lateinit var onlineProvider: OnlineRssDataProvider

    @Bean
    protected lateinit var offlineProvider: OfflineRssDataProvider

    private var provider: RssDataProvider? = null
        get() = if (netUtils.isOnline()) onlineProvider else offlineProvider


    /**
     * Loads rss data
     *
     * @return observable with loaded FeedItems
     */
    fun loadRssData(): Observable<List<FeedItem>> =
            provider!!.loadData()
}
