package code.example.rssreader.b.util

import android.content.Context
import org.androidannotations.annotations.EBean
import org.androidannotations.annotations.RootContext

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

/**
 * Date utilities implementation
 */
@EBean
open class DateUtils {

    @RootContext
    protected lateinit var context: Context

    /**
     * Converts rss publication date string to user format string
     *
     * @param rssItemPubDate rss publication date string
     * @return user formatted publication date string
     */
    fun convertPubDateToUserFormat(rssItemPubDate: String): String {
        val pubDate = convertPubDateStringToDate(rssItemPubDate)

        //Get user date format
        val dateFormat = android.text.format.DateFormat.getDateFormat(context)

        //Get user time format
        val timeFormat = android.text.format.DateFormat.getTimeFormat(context)

        return dateFormat.format(pubDate) + " " + timeFormat.format(pubDate)
    }

    /**
     * Converts rss publication date string to Date
     *
     * @param pubDate publication date string
     * @return publication date
     */
    fun convertPubDateStringToDate(pubDate: String): Date? =
            try {
                val dateFormat = SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH)
                dateFormat.parse(pubDate)

            } catch (e: ParseException) {
                e.printStackTrace()
                null
            }
}
