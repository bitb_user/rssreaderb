package code.example.rssreader.b.api

import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Url
import rx.Observable

interface ApiFile {

    @GET
    fun downloadFile(@Url fileUrl: String): Observable<ResponseBody>
}
