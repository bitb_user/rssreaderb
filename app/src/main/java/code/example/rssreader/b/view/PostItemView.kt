package code.example.rssreader.b.view

import android.content.Context
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.widget.TextView
import code.example.rssreader.b.R
import code.example.rssreader.b.model.rss.FeedItem
import org.androidannotations.annotations.EViewGroup
import org.androidannotations.annotations.ViewById

@EViewGroup(R.layout.view_post_item)
open class PostItemView : CardView {

    @ViewById(R.id.imageView_thumbnail)
    protected lateinit var thumbnailImageView: LoadableImageView

    @ViewById(R.id.textView_title)
    protected lateinit var titleTextView: TextView

    @ViewById(R.id.textView_description)
    protected lateinit var descriptionTextView: TextView

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    init {
        val attrs = intArrayOf(android.R.attr.selectableItemBackground)
        val ta = context.obtainStyledAttributes(attrs)
        val drawableFromTheme = ta.getDrawable(0)
        ta.recycle()
        foreground = drawableFromTheme
        radius = 1f
    }

    fun setFeedItem(feedItem: FeedItem) {
        thumbnailImageView.setUrl(feedItem.enclosure?.url)
        titleTextView.text = feedItem.title
        descriptionTextView.text = feedItem.description
    }
}
