package code.example.rssreader.b.util

import android.content.Context
import android.net.ConnectivityManager
import org.androidannotations.annotations.EBean
import org.androidannotations.annotations.RootContext

/**
 * Net utilities
 */
@EBean
open class NetUtils {

    @RootContext
    protected lateinit var context: Context

    /**
     * Checks device online status
     *
     * @return device true if device online
     */
    fun isOnline(): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = connectivityManager.activeNetworkInfo
        return netInfo?.isConnectedOrConnecting == true
    }
}
