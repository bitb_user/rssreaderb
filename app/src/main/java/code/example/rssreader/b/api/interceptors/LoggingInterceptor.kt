package code.example.rssreader.b.api.interceptors

import android.util.Log
import okhttp3.Interceptor
import okhttp3.RequestBody
import okhttp3.Response
import okio.Buffer
import org.androidannotations.annotations.EBean
import java.io.IOException


@EBean
open class LoggingInterceptor : Interceptor {

    private val tag = "LoggingInterceptor"

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val starting = System.currentTimeMillis()
        val request = chain.request()
        val reqBody = bodyToString(request.body())
        Log.d(tag, "REQUEST : $request" + if (reqBody.isNotEmpty()) "\nBody:\n$reqBody" else "")
        val response = chain.proceed(request)
        Log.d(tag, "RESPONSE: $response\nTIMING: ${System.currentTimeMillis() - starting} ms")
        if (!response.isSuccessful) {
            Log.e(tag, response.message())
        }
        return response
    }

    private fun bodyToString(request: RequestBody?): String {
        if (request == null) return ""
        return try {
            val buffer = Buffer()
            request.writeTo(buffer)
            buffer.readUtf8()
        } catch (e: IOException) {
            Log.e(tag, e.message, e)
            String()
        }
    }
}