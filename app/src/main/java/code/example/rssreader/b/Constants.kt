package code.example.rssreader.b

/**
 * Application constants
 */
class Constants {

    interface Db {
        companion object {
            const val DB_FILE_NAME = "RssReaderDB.sqlite"
            const val DB_VERSION = 1
        }
    }

    interface SharedPrefs {
        companion object {
            const val SHARED_PREFS_FILE_NAME = "prefPS"
            const val RSS_HOST_KEY = "rssHost"
        }
    }

    interface Api {
        companion object {
            const val CONNECT_TIMEOUT = 10 //seconds
            const val READ_TIMEOUT = 30 //seconds
        }
    }

    interface Settings {
        companion object {
            const val DEFAULT_RSS_HOST = "https://lenta.ru/rss/"
        }
    }
}
