package code.example.rssreader.b.domain

import android.content.Context
import android.content.SharedPreferences
import code.example.rssreader.b.Constants
import org.androidannotations.annotations.EBean
import org.androidannotations.annotations.RootContext


/**
 * Shared preferences manager
 */
@EBean
open class SharedPrefsManager() {

    @RootContext
    lateinit var context: Context

    private val preferences: SharedPreferences
        get() = context.getSharedPreferences(Constants.SharedPrefs.SHARED_PREFS_FILE_NAME, Context.MODE_PRIVATE)

    fun getInt(key: String, defValue: Int) = preferences.getInt(key, defValue)
    fun setInt(key: String, value: Int) {
        preferences.edit().putInt(key, value).apply()
    }

    fun getBoolean(key: String, defValue: Boolean) = preferences.getBoolean(key, defValue)
    fun setBool(key: String, value: Boolean) {
        preferences.edit().putBoolean(key, value).apply()
    }

    fun getFloat(key: String, defValue: Float) = preferences.getFloat(key, defValue)
    fun setFloat(key: String, value: Float) {
        preferences.edit().putFloat(key, value).apply()
    }

    fun getString(key: String, defValue: String): String? = preferences.getString(key, defValue)
    fun setString(key: String, value: String) {
        preferences.edit().putString(key, value).apply()
    }
}
