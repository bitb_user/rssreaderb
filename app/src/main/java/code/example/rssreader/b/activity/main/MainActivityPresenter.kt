package code.example.rssreader.b.activity.main

import code.example.rssreader.b.Navigator
import code.example.rssreader.b.domain.rss.RssFeedManager
import code.example.rssreader.b.model.rss.FeedItem
import code.example.rssreader.b.model.rx.rxIo
import code.example.rssreader.b.util.DateUtils
import com.arellomobile.mvp.InjectViewState
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EBean

/**
 * Main activity presenter
 */
@EBean
open class MainActivityPresenter : MainActivityContract.Presenter {

    @Bean
    protected lateinit var rssFeedManager: RssFeedManager

    @Bean
    protected lateinit var navigator: Navigator

    override var view: MainActivityContract.View? = null

    override fun loadData() {
        loadRssData({ busy: Boolean -> view?.setBusy(busy) })
    }

    override fun refreshData() {
        loadRssData({ busy: Boolean -> view?.setRefreshBusy(busy) })
    }

    override fun showSettings() {
        navigator.gotoSettingsActivity()
    }

    override fun showPostPreview(feedItem: FeedItem) {
        navigator.gotoPostPreviewActivity(feedItem)
    }

    private fun loadRssData(busyUnit: (busy: Boolean) -> Unit) {
        busyUnit(true)
        rxIo(rssFeedManager.loadRssData(),
                onFinished = { busyUnit(false) },
                onError = { view?.showSnackBarMessage(it?.message ?: return@rxIo) },
                onNext = { view?.setRssItems(it ?: listOf()) })
    }
}

