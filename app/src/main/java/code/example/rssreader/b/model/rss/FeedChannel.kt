package code.example.rssreader.b.model.rss

import org.simpleframework.xml.Root
import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import java.io.Serializable

/**
 * Rss feed channel data model class
 */
@Root(name = "channel", strict = false)
class FeedChannel: Serializable {

        @field:Element(name = "title")
        var title: String? = null

        @field:Element(name = "description")
        var description: String? = null

        @field:ElementList(inline = true, name = "item")
        var feedItems: List<FeedItem>? = null
}
