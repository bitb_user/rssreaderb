package code.example.rssreader.b.activity.settings

import code.example.rssreader.b.Constants
import code.example.rssreader.b.R
import code.example.rssreader.b.domain.SharedPrefsManager
import code.example.rssreader.b.domain.rss.RssHostManager
import code.example.rssreader.b.model.rx.rxIo
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EBean
import org.androidannotations.annotations.res.StringRes

/**
 * Settings fragment presenter implementation
 */
@EBean
open class SettingsActivityPresenter : SettingsActivityContract.Presenter {

    @Bean
    protected lateinit var rssHostManager: RssHostManager

    @StringRes(R.string.message_rss_host_is_not_specified)
    protected lateinit var notSpecifiedRssHost: String

    @StringRes(R.string.message_invalid_rss_host)
    protected lateinit var invalidRssHost: String

    @StringRes(R.string.message_valid_rss_host)
    protected lateinit var validRssHost: String

    @StringRes(R.string.message_unknown_error)
    protected lateinit var unknownError: String

    override var view: SettingsActivityContract.View? = null

    /**
     * Loads saved rss host url from shared preferences
     */
    override fun loadSavedRssHost() {
        view?.setRssHostUrl(rssHostManager.savedRssHost)
    }

    /**
     * Saves rss host url to shared preferences
     *
     * @param hostUrl rss host url to save
     */
    override fun saveRssHost(hostUrl: String) {
        rssHostManager.saveRssHost(hostUrl)
    }

    /**
     * Checks rss host validity
     *
     * @param hostUrl rss host url to check
     */
    override fun checkRssHost(hostUrl: String) {
        view?.setBusy(true)
        rxIo(rssHostManager.checkRssHost(hostUrl),
                onFinished = { view?.setBusy(false) },
                onError = { view?.showSnackBarMessage(it?.message ?: unknownError) },
                onNext = { hostStatus ->
                    when (hostStatus) {
                        RssHostManager.HostStatus.NOT_SPECIFIED -> {
                            view?.showSnackBarMessage(notSpecifiedRssHost)
                        }
                        RssHostManager.HostStatus.VALID -> {
                            view?.showSnackBarMessage(validRssHost)
                        }
                        else -> {
                            view?.showSnackBarMessage(invalidRssHost)
                        }
                    }
                }
        )
    }
}
