package code.example.rssreader.b.view

import android.content.Context
import android.net.Uri
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import code.example.rssreader.b.R
import code.example.rssreader.b.domain.MediaDataProvider
import code.example.rssreader.b.model.rx.rxIo
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EViewGroup
import org.androidannotations.annotations.ViewById
import pl.droidsonroids.gif.GifImageView

@EViewGroup(R.layout.view_loadable_image)
open class LoadableImageView : RelativeLayout {

    @ViewById(R.id.gifImageView)
    lateinit var gifImageView: GifImageView

    @ViewById(R.id.progressBar_image)
    lateinit var progressBar: ProgressBar

    @Bean
    protected lateinit var mediaDataProvider: MediaDataProvider

    private var scaleType = ImageView.ScaleType.FIT_CENTER

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    @AfterViews
    fun afterViews() {
        initScaleType()
    }

    fun setUrl(url: String?) {
        gifImageView.setImageDrawable(null)

        if (url.isNullOrEmpty()) {
            gifImageView.setImageResource(R.drawable.no_image)
            return
        }

        progressBar.visibility = View.VISIBLE
        rxIo(mediaDataProvider.loadFileUri(url),
                onFinished = { progressBar.visibility = View.GONE },
                onNext = {
                    gifImageView.setImageURI(Uri.parse(it.toString()))
                }
        )
    }

    fun setScaleType(scaleType: ImageView.ScaleType) {
        gifImageView.scaleType = scaleType
    }

    private fun initScaleType() {
        gifImageView.scaleType = scaleType
    }

    private fun init(context: Context, attrs: AttributeSet) {
        val ta = context.obtainStyledAttributes(attrs, R.styleable.LoadableImageView, 0, 0)
        try {
            if (ta.hasValue(R.styleable.LoadableImageView_scaleType)) {
                val scaleTypeIndex = ta.getInt(R.styleable.LoadableImageView_scaleType, ImageView.ScaleType.FIT_CENTER.ordinal)
                scaleType = ImageView.ScaleType.values()[scaleTypeIndex]
            }
        } finally {
            ta.recycle()
        }
    }
}
