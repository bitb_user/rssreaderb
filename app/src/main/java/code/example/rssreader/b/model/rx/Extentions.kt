package code.example.rssreader.b.model.rx

import rx.Observable
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

fun <T> rxIo(observable: Observable<T>,
             onError: (e: Throwable?) -> Unit = {},
             onNext: (o: T?) -> Unit = {},
             onComplete: () -> Unit = {},
             onFinished: () -> Unit = {}) {
    observable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<T> {
                override fun onCompleted() {
                    onComplete()
                    onFinished()
                }

                override fun onNext(t: T?) {
                    onNext(t)
                }

                override fun onError(e: Throwable?) {
                    e?.printStackTrace()
                    onFinished()
                    onError(e)
                }
            })
}