package code.example.rssreader.b.domain.persisters;

import code.example.rssreader.b.model.rss.Enclosure;

public class EnclosurePersister extends JsonPersister<Enclosure> {

    private static EnclosurePersister instance;

    public static EnclosurePersister getSingleton() {
        if (instance == null) {
            instance = new EnclosurePersister();
        }
        return instance;
    }

    private EnclosurePersister() {
        super(Enclosure.class);
    }
}


