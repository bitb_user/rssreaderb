package code.example.rssreader.b.adapter

import android.support.v7.widget.RecyclerView
import android.view.View

class ViewWrapper<V : View>(val view: V, private val onAdapterItemClickUnit: (position: Int) -> Unit)
    : RecyclerView.ViewHolder(view) {

    init {
        view.setOnClickListener { onAdapterItemClickUnit(adapterPosition) }
    }
}
