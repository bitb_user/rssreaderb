package code.example.rssreader.b.domain.files

import android.util.Log
import code.example.rssreader.b.api.ApiRoot
import okhttp3.ResponseBody
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EBean
import rx.Observable
import java.io.*
import java.net.URI

@EBean
open class FileLoader {

    companion object {
        const val TAG = "FileLoader"
        const val UNABLE_TO_SAVE_FILE = "Unable to save file"
    }

    @Bean
    protected lateinit var apiRoot: ApiRoot

    fun loadFile(url: String, filePath: String): Observable<URI?> =
            apiRoot.apiFile.downloadFile(url)
                    .map { responseBody ->
                        val fileSaved = saveFileFromResponseBody(responseBody, filePath)
                        if (fileSaved) URI(filePath)
                        else null
                    }

    private fun saveFileFromResponseBody(responseBody: ResponseBody, filePath: String): Boolean =
            try {
                val file = File(filePath)
                var inputStream: InputStream? = null
                var outputStream: OutputStream? = null

                try {
                    val fileReader = ByteArray(4096)

                    val fileSize = responseBody.contentLength()
                    var fileSizeDownloaded: Long = 0

                    inputStream = responseBody.byteStream()
                    outputStream = FileOutputStream(file)

                    while (true) {
                        val read = inputStream.read(fileReader)
                        if (read == -1) break

                        outputStream.write(fileReader, 0, read)

                        fileSizeDownloaded += read

                        if (fileSizeDownloaded == fileSize) {
                            val message = "File %s downloaded. Size(%s)".format(filePath, fileSizeDownloaded)
                            Log.d(TAG, message)
                        }
                    }
                    outputStream.flush()
                    true

                } catch (e: IOException) {
                    Log.e(TAG, UNABLE_TO_SAVE_FILE, e)
                    false

                } finally {
                    inputStream?.close()
                    outputStream?.close()
                }

            } catch (e: IOException) {
                Log.e(TAG, UNABLE_TO_SAVE_FILE, e)
                false
            }

}
