package code.example.rssreader.b.activity

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.arellomobile.mvp.MvpAppCompatActivity

/**
 * Base activity class
 */
abstract class BaseActivity : MvpAppCompatActivity() {

    abstract var toolbarTitle: String

    private var currentSnackBar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.title = toolbarTitle
    }

    fun closeSoftKeyboard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(currentFocus.windowToken, 0)
    }

    fun showSnackBarMessage(message: String) {
        if (currentSnackBar?.isShown == true) {
            currentSnackBar?.setText(message)
            currentSnackBar?.show()
            return
        }

        val parentLayout = findViewById<View>(android.R.id.content)
        currentSnackBar = Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG)
        currentSnackBar?.show()
        Log.e("Message", message)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
