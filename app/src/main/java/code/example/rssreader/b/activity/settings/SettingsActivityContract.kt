package code.example.rssreader.b.activity.settings

interface SettingsActivityContract {

    interface View {

        var presenter: Presenter

        fun setRssHostUrl(url: String)

        fun showSnackBarMessage(message: String)

        fun setBusy(busy: Boolean)
    }

    interface Presenter {

        var view: View?

        fun loadSavedRssHost()

        fun saveRssHost(hostUrl: String)

        fun checkRssHost(hostUrl: String)
    }
}