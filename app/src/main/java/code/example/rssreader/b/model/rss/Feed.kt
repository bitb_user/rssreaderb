package code.example.rssreader.b.model.rss

import org.simpleframework.xml.Root
import org.simpleframework.xml.Element
import java.io.Serializable

/**
 * Rss feed data model class
 */
@Root(name = "rss", strict = false)
class Feed : Serializable {

    @field:Element(name = "channel")
    var channel: FeedChannel? = null

    var url: String? = null
}