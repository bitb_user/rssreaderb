package code.example.rssreader.b.domain.rss.providers

import code.example.rssreader.b.api.ApiRoot
import code.example.rssreader.b.dataAccess.DatabaseManager
import code.example.rssreader.b.domain.rss.RssHostManager
import code.example.rssreader.b.model.rss.FeedItem
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EBean
import rx.Observable

@EBean
open class OnlineRssDataProvider : RssDataProvider {

    @Bean
    protected lateinit var apiRoot: ApiRoot

    @Bean
    protected lateinit var hostManager: RssHostManager

    @Bean
    protected lateinit var databaseManager: DatabaseManager

    override fun loadData(): Observable<List<FeedItem>> =
            apiRoot.apiRss.getFeed(hostManager.savedRssHost)
                    .map { it.channel?.feedItems ?: listOf() }
                    .doOnNext { databaseManager.createOrUpdate(it, FeedItem::class.java) }
}