package code.example.rssreader.b.domain.rss.providers

import code.example.rssreader.b.dataAccess.DatabaseManager
import code.example.rssreader.b.model.rss.FeedItem
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EBean
import rx.Observable

@EBean
open class OfflineRssDataProvider : RssDataProvider {

    @Bean
    protected lateinit var databaseManager: DatabaseManager

    override fun loadData(): Observable<List<FeedItem>> =
            Observable.fromCallable { databaseManager.getList(FeedItem::class.java) }
}