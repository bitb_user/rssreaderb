package code.example.rssreader.b.model.rss

import code.example.rssreader.b.domain.persisters.EnclosurePersister
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root
import java.io.Serializable


/**
 * Rss feed item data model class
 */
@DatabaseTable(tableName = "rssItem")
@Root(name = "item", strict = false)
class FeedItem : Serializable {

    @DatabaseField(id = true)
    @field:Element(name = "guid")
    var guid: String? = null

    @DatabaseField
    @field:Element(name = "title")
    var title: String? = null

    @DatabaseField
    @field:Element(name = "link")
    var link: String? = null

    @DatabaseField
    @field:Element(name = "description")
    var description: String? = null

    @DatabaseField
    @field:Element(name = "pubDate")
    var pubDate: String? = null

    @DatabaseField
    @field:Element(name = "author", required = false)
    var author: String? = null

    @DatabaseField(persisterClass = EnclosurePersister::class)
    @field:Element(name = "enclosure", required = false)
    var enclosure: Enclosure? = null

    override fun toString(): String {
        return "RssItem{" +
                "guid=" + guid +
                ", title='" + title + '\''.toString() +
                ", link='" + link + '\''.toString() +
                ", description='" + description + '\''.toString() +
                ", pubDate=" + pubDate +
                ", author='" + author + '\''.toString() +
                ", enclosure='" + enclosure + '\''.toString() +
                '}'
    }
}
