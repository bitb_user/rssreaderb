package code.example.rssreader.b.activity.postBrowse

import android.webkit.WebView
import android.webkit.WebViewClient
import code.example.rssreader.b.R
import code.example.rssreader.b.activity.BaseActivity
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.EActivity
import org.androidannotations.annotations.Extra
import org.androidannotations.annotations.ViewById
import org.androidannotations.annotations.res.StringRes

/**
 * Post browse activity class
 */
@EActivity(R.layout.activity_post_browse)
open class PostBrowseActivity : BaseActivity() {

    @StringRes(R.string.title_post_browse)
    override lateinit var toolbarTitle: String

    @Extra
    @JvmField
    protected var postUrl: String? = null

    @ViewById(R.id.webView_post)
    protected lateinit var postWebView: WebView

    @AfterViews
    protected fun afterViews() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        loadPostUrl(postUrl ?: return)
    }

    private fun loadPostUrl(url: String) {
        postWebView.webViewClient = WebViewClient() //To avoid WebView to launch the default browser
        postWebView.loadUrl(url)
    }
}