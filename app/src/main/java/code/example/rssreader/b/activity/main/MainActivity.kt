package code.example.rssreader.b.activity.main

import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.RelativeLayout
import code.example.rssreader.b.R
import code.example.rssreader.b.activity.BaseActivity
import code.example.rssreader.b.adapter.PostsListAdapter
import code.example.rssreader.b.model.rss.FeedItem
import org.androidannotations.annotations.*
import org.androidannotations.annotations.res.StringRes

/**
 * Main activity class
 */
@EActivity(R.layout.activity_main)
@OptionsMenu(R.menu.menu_main)
open class MainActivity : BaseActivity(), MainActivityContract.View {

    @ViewById(R.id.swipeRefreshLayout_refreshLayout)
    protected lateinit var swipeRefreshLayout: SwipeRefreshLayout

    @ViewById(R.id.recycleView_postsList)
    protected lateinit var recyclerView: RecyclerView

    @ViewById(R.id.progressView)
    protected lateinit var progressView: RelativeLayout

    @StringRes(R.string.app_name)
    override lateinit var toolbarTitle: String

    @Bean
    protected lateinit var adapter: PostsListAdapter

    @Bean(MainActivityPresenter::class)
    override lateinit var presenter: MainActivityContract.Presenter

    @JvmField
    @InstanceState
    var feedItems: ArrayList<FeedItem>? = null

    @AfterViews
    protected fun afterViews() {
        setupAdapter()
        setupSwipeRefresh()
        presenter.view = this
        loadData()
    }

    @OptionsItem(R.id.action_settings)
    protected fun onSettingsClick() {
        presenter.showSettings()
    }

    override fun setRssItems(feedItems: List<FeedItem>) {
        adapter.items = feedItems.toMutableList()
        this.feedItems = ArrayList(feedItems)
    }

    override fun setBusy(busy: Boolean) {
        progressView.visibility = if (busy) View.VISIBLE else View.GONE
    }

    override fun setRefreshBusy(busy: Boolean) {
        swipeRefreshLayout.isRefreshing = busy
    }

    private fun setupAdapter() {
        adapter.onItemClickUnit = { presenter.showPostPreview(it) }
        recyclerView.adapter = adapter
    }

    private fun setupSwipeRefresh() {
        swipeRefreshLayout.setOnRefreshListener { presenter.refreshData() }
    }

    private fun loadData() {
        if (feedItems != null) {
            adapter.items = feedItems!!.toMutableList()
            return
        }
        presenter.loadData()
    }
}
