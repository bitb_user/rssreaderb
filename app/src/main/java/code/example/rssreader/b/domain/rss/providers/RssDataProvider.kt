package code.example.rssreader.b.domain.rss.providers

import code.example.rssreader.b.model.rss.FeedItem
import rx.Observable

interface RssDataProvider {

    fun loadData(): Observable<List<FeedItem>>
}