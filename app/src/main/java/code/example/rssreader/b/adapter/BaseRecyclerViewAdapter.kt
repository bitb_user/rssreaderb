package code.example.rssreader.b.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import java.util.*

abstract class BaseRecyclerViewAdapter<T, V : View> : RecyclerView.Adapter<ViewWrapper<V>>() {

    var onItemClickUnit: (item: T) -> Unit = {}
    var items: MutableList<T> = mutableListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    fun addItems(itemsToAdd: List<T>) {
        items.addAll(itemsToAdd)
        notifyItemRangeInserted(items.size, itemsToAdd.size)
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewWrapper<V> =
            ViewWrapper(onCreateItemView(parent, viewType), { onItemClickUnit(items[it]) })

    protected abstract fun onCreateItemView(parent: ViewGroup, viewType: Int): V
}
