package code.example.rssreader.b.model.rss

import org.simpleframework.xml.Attribute
import java.io.Serializable

class Enclosure : Serializable {

    @field:Attribute(name = "url")
    var url: String? = null

    @field:Attribute(name = "length")
    var length: Long = 0

    @field:Attribute(name = "type")
    var type: String? = null
}