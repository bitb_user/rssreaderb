package code.example.rssreader.b.domain

import code.example.rssreader.b.domain.files.FileManager
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EBean
import rx.Observable
import java.net.URI

@EBean
open class MediaDataProvider {

    @Bean
    protected lateinit var fileManager: FileManager

    fun loadFileUri(fileUrl: String?): Observable<URI?> {
        if (fileUrl == null)
            return Observable.fromCallable { null }

        if (fileManager.isFileExists(fileUrl)) {
            val localFilePath = fileManager.getFilePath(fileUrl)
            return Observable.fromCallable { URI.create(localFilePath) }
        }

        return fileManager.loadFile(fileUrl, fileUrl)
    }
}
