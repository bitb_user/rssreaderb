package code.example.rssreader.b.activity.postPreview

import code.example.rssreader.b.Navigator
import code.example.rssreader.b.model.rss.FeedItem
import code.example.rssreader.b.util.DateUtils
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EBean


@EBean
open class PostPreviewActivityPresenter : PostPreviewActivityContract.Presenter {

    @Bean
    protected lateinit var dateUtils: DateUtils

    @Bean
    protected lateinit var navigator: Navigator

    override var view: PostPreviewActivityContract.View? = null

    override fun proceedData(feedItem: FeedItem) {
        view?.setTitle(feedItem.title ?: String())
        view?.setAuthor(feedItem.author ?: String())
        view?.setDescription(feedItem.description ?: String())
        view?.setImageUrl(feedItem.enclosure?.url)

        val formattedDate = dateUtils.convertPubDateToUserFormat(feedItem.pubDate ?: String())
        view?.setPublicationDate(formattedDate)
    }

    override fun browseRssPost(postUrl: String) {
        navigator.gotoPostBrowseActivity(postUrl)
    }
}