package code.example.rssreader.b.domain.rss

import code.example.rssreader.b.Constants
import code.example.rssreader.b.domain.SharedPrefsManager
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EBean
import org.xml.sax.InputSource
import rx.Observable
import java.net.URL
import javax.xml.parsers.DocumentBuilderFactory

/**
 * Rss host manager
 */
@EBean
open class RssHostManager {

    enum class HostStatus { NOT_SPECIFIED, INVALID, VALID }

    @Bean
    protected lateinit var sharedPrefsManager: SharedPrefsManager

    val savedRssHost: String
        get() = sharedPrefsManager.getString(Constants.SharedPrefs.RSS_HOST_KEY,
                Constants.Settings.DEFAULT_RSS_HOST) ?: String()

    /**
     * Saves rss host url to shared preferences
     *
     * @param rssHost url to save
     */
    fun saveRssHost(rssHost: String) {
        sharedPrefsManager.setString(Constants.SharedPrefs.RSS_HOST_KEY, rssHost)
    }

    /**
     * Async checks rss host validity
     *
     * @param hostUrl              rss host url to check
     * @return host status observable
     */
    fun checkRssHost(hostUrl: String): Observable<HostStatus> {
        return Observable.fromCallable {
            val url = URL(hostUrl)
            val dbf = DocumentBuilderFactory.newInstance()
            val db = dbf.newDocumentBuilder()
            val doc = db.parse(InputSource(url.openStream()))
            doc.documentElement.normalize()

            val root = doc.documentElement
            val rootTagName = root.tagName

            if (rootTagName.isNullOrEmpty())
                HostStatus.NOT_SPECIFIED
            else if (rootTagName.equals("rss", ignoreCase = true))
                HostStatus.VALID
            else
                HostStatus.INVALID
        }
    }
}
