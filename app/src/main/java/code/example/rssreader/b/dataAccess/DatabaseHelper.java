package code.example.rssreader.b.dataAccess;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.EBean;

import java.sql.SQLException;

import code.example.rssreader.b.Constants;
import code.example.rssreader.b.model.rss.FeedItem;


/**
 * OrmLite database helper
 */
@EBean
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    // Classes added to DB
    private static final ClassPref[] CLASSES = new ClassPref[]{
            //Second parameter  - deleting table from DB when DB version was changed
            new ClassPref(FeedItem.class, true),
    };

    public DatabaseHelper(Context context) {
        super(context, Constants.Db.DB_FILE_NAME, null, Constants.Db.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connection) {
        try {
            for (ClassPref classPref : CLASSES)
                TableUtils.createTableIfNotExists(connection, classPref.clazz);        //Creating table for class
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connection, int oldVersion, int newVersion) {
        try {
            for (ClassPref classPref : CLASSES) {
                if (classPref.dropOnUpgrade)
                    TableUtils.dropTable(connection, classPref.clazz, true);        // Delete table of class

                TableUtils.createTableIfNotExists(connection, classPref.clazz);        // Create table for class if not exists
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private static class ClassPref {
        Class<?> clazz;
        boolean dropOnUpgrade;

        ClassPref(Class<?> clazz, boolean dropOnUpgrade) {
            this.clazz = clazz;
            this.dropOnUpgrade = dropOnUpgrade;
        }
    }
}

