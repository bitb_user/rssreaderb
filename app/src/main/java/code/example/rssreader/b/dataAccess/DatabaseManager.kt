package code.example.rssreader.b.dataAccess

import android.content.Context
import android.util.Log
import com.j256.ormlite.dao.Dao
import com.j256.ormlite.table.TableUtils
import org.androidannotations.annotations.EBean
import org.androidannotations.annotations.RootContext
import java.sql.SQLException
import java.util.*

/**
 * Database manager
 */
@EBean
open class DatabaseManager() {

    @RootContext
    protected lateinit var context: Context

    private val TAG = "DatabaseManager"
    private val databaseHelper: DatabaseHelper
        get() = DatabaseHelper(context)


    // ////////////////////////////////////////////////////////////////////////////////////////////
    // ADD METHODS
    // ////////////////////////////////////////////////////////////////////////////////////////////

    fun <T> create(data: T?, clazz: Class<T>) {
        if (data == null)
            return

        try {
            val dao = databaseHelper.getDao(clazz)
            dao.create(data)
        } catch (e: Exception) {
            Log.e(TAG, "Exception while creating db object", e)
        }

    }

    fun <T> createOrUpdate(data: T?, clazz: Class<T>) {
        if (data == null)
            return

        try {
            val dao = databaseHelper.getDao(clazz)
            dao.createOrUpdate(data)
        } catch (e: Exception) {
            Log.e(TAG, "Exception while creating or updating db object", e)
        }

    }

    fun <T> createOrUpdate(data: List<T>?, clazz: Class<T>) {
        if (data == null || data.isEmpty())
            return

        try {
            val dao = databaseHelper.getDao<Dao<T, Int>, T>(clazz)
            dao.callBatchTasks {
                for (item in data) {
                    dao.createOrUpdate(item)
                }
                null
            }
        } catch (e: Exception) {
            Log.e(TAG, "Exception while creating or updating list of db objects", e)
        }

    }

    // ////////////////////////////////////////////////////////////////////////////////////////////
    // EXTRACT METHODS
    // ////////////////////////////////////////////////////////////////////////////////////////////

    fun <T, E> getForId(id: E, clazz: Class<T>): T? {
        try {
            val dao = databaseHelper.getDao<Dao<T, E>, T>(clazz)
            return dao.queryForId(id)
        } catch (e: Exception) {
            Log.e(TAG, "Exception while getting db object by id", e)
            return null
        }

    }

    fun <T> getList(clazz: Class<T>): List<T> {
        val result: List<T>
        try {
            val dao = databaseHelper.getDao(clazz)
            result = dao.queryForAll()
        } catch (e: Exception) {
            Log.e(TAG, "Exception while getting list of db objects", e)
            return emptyList<T>()
        }

        return result
    }

    fun <T> getListField(clazz: Class<T>, fieldName: String): List<String> {
        val result = ArrayList<String>()
        try {
            val dao = databaseHelper.getDao(clazz)
            val raws = dao.queryRaw("SELECT DISTINCT " + fieldName
                    + " FROM " + clazz.simpleName)

            for (strings in raws)
                result.add(strings[0])

        } catch (e: Exception) {
            Log.e(TAG, "Exception while getting field list of db objects", e)
            return emptyList<String>()
        }

        return result
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////
    // DELETE METHODS
    // ////////////////////////////////////////////////////////////////////////////////////////////

    fun <T> removeObjectsByColumn(columnName: String?, value: Any,
                                  clazz: Class<T>) {
        if (columnName == null)
            return
        try {
            val dao = databaseHelper.getDao<Dao<T, String>, T>(clazz)
            dao.callBatchTasks {
                val deleteBuilder = dao.deleteBuilder()
                deleteBuilder.where().eq(columnName, value)
                deleteBuilder.delete()
                null
            }
        } catch (e: Exception) {
            Log.e(TAG, "Exception while removing db object by column", e)
        }

    }

    fun <T> clearTable(clazz: Class<T>) {
        try {
            TableUtils.clearTable(databaseHelper.connectionSource, clazz)
        } catch (e: SQLException) {
            Log.e(TAG, "Exception while clearing db table", e)
        }

    }
}
